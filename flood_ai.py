import pdb
import random
import sys

from collections import namedtuple

from itertools import product

import networkx as nx  # type: ignore

import numpy as np  # type: ignore

from skimage.measure import label  # type: ignore

from typing import (
    List,
    Tuple,
)

sys.path.append('aima-python')
from search import (  # type: ignore
    Problem,
    astar_search,
)


State1 = namedtuple('State', 'board nr_turns')


class State(State1):
    def __hash__(self):
        return hash(tuple(self.board.flatten()))
    def __eq__(self, other):
        return tuple(self.board.flatten()) == tuple(other.board.flatten())
    def __lt__(self, other):
        return tuple(self.board.flatten()) < tuple(other.board.flatten())


NEIGHBOURS = [(-1, 0), (0, -1), (0, +1), (+1, 0)]


def init_board(width, height, nr_colors, seed):
    random.seed(seed)
    colors = range(nr_colors)
    return np.vstack([
        [
            random.choice(colors)
            for _ in range(width)
        ]
        for _ in range(height)
    ])


class FloodProblem(Problem):
    def __init__(self, board, nr_turns):
        self.height, self.width = board.shape
        self.colors = np.unique(board)
        self.state = State(board=board, nr_turns=nr_turns)
        self.initial = self.state

    def _get_neightbours(self, pos):
        for dx, dy in NEIGHBOURS:
            yield pos[0] + dx, pos[1] + dy

    def _flood_fill(self, board, pos, old_color, new_color):
        if old_color == new_color:
            return
        if not 0 <= pos[0] < self.height or not 0 <= pos[1] < self.width:
            return
        if board[pos[0]][pos[1]] != old_color:
            return
        board[pos[0]][pos[1]] = new_color
        for new_pos in self._get_neightbours(pos):
            self._flood_fill(board, new_pos, old_color, new_color)

    def actions(self, state):
        return self.colors

    def result(self, state, action):
        new_board = state.board.copy()
        self._flood_fill(
            board=new_board,
            pos=(0, 0),
            old_color=state.board[0][0],
            new_color=action,
        )
        new_nr_turns = state.nr_turns - 1
        return State(
            board=new_board,
            nr_turns=new_nr_turns,
        )

    def goal_test(self, state):
        if len(np.unique(state.board)) == 1:
            print('Congrats -- you won!')
            return True
        elif state.nr_turns == 0:
            print('Sorry -- you lost!')
            print('No more turns left')
            return True
        else:
            return False

    def path_cost(self, c, state1, action, state2):
        return c + 1


def display(state):
    print('Number of turns left:', state.nr_turns)
    print('\n'.join(' '.join(str(n) for n in line) for line in state.board))


def user_game(problem):
    state = problem.state
    display(state)
    while not problem.goal_test(state):
        action = int(input('Pick a number: '))
        state = problem.result(state, action)
        display(state)


def greedy_game():
    pass


def astar_game(problem):
    def labels_to_graph(labels):
        edges = []  # type: List[Tuple[int, int]]
        nr_rows, nr_cols = labels.shape
        for i, j in product(range(nr_rows), range(nr_cols)):
            label_1 = labels[i, j]
            for dx, dy in NEIGHBOURS:
                if 0 <= i + dx < nr_rows and 0 <= j + dy < nr_cols:
                    label_2 = labels[i + dx, j + dy]
                    edges.append((label_1, label_2))
        return nx.Graph(edges)

    def heuristic(node):
        labels = label(node.state.board, background=-1, connectivity=1)
        graph = labels_to_graph(labels)
        dists = nx.single_source_shortest_path_length(graph, source=1)
        max_dist = max(dists.values())
        nr_colors = len(np.unique(node.state.board))
        nr_moves_lower_bound = max(max_dist, nr_colors)
        return nr_moves_lower_bound

    result = astar_search(problem, h=heuristic)
    display(problem.state)

    for action, node in zip(result.solution(), result.path()[1:]):
        print("Selected action:", action)
        display(node.state)
        pdb.set_trace()


def main():
    board = init_board(15, 15, 3, 1337)
    problem = FloodProblem(board, nr_turns=10)
    # user_game(problem)
    astar_game(problem)


if __name__ == '__main__':
    main()
