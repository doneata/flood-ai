An AI for the [Flood it](http://cdn.labpixies.com/campaigns/flood/flood.html) game.

# Setting up

```
#!bash
python3 -m venv venv
pip install -r requirements.txt
git clone git@github.com:aimacode/aima-python.git
```

# Playing the game

```
#!bash
python flood_ai.py
```
